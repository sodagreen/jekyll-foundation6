Темплейт Jekyll + Zurb Foundation 6 для Gitlab pages. Сборка с помощью Gulp.

Дополнительно установленные компоненты:

* Gulp
    * gulp-css-globbing
* scss:
    * susy
    * sass-breakpoint

Локальная установка: ```bundle install```, ```npm install```, ```bower install```, ```npm start```

Все ассеты собираются локально и коммитятся уже готовыми на сервер, собираются командой ```npm build```. Перед деплоем убедитесь, что uncss не удалил лишние классы.
